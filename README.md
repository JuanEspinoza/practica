**PASOS PARA LA INSTALACIÓN** 

 1. Iniciar el proyecto en npm

	 - npm init 
 2. Almacenar React como dependencia del proyecto para desarrollo y publicación del paquete

	 - npm install --save react

3. Agregar la dependecia parcel-bundler al proyecto como dependencia de desarrollo

	 - yarn add --dev  parcel-bundler
	
	o bien si el proyecto es administrado por npm utilizar el siguiente comando:
		- npm install --save-dev parcel-bundler

	  Si yarn no está instalado por defecto en nuestro npm, ejecutar el sig. comando 

		- npm i -g yarn

4. Agregar un archivo *index.html* a nuestro proyecto y agregar un cuerpo sencillo
	
5. Definir en el elemento de scripts dentro del archivo *package.json*  la ruta que ejecutará el parcel-bundler al momento de la ejecución del archivo


	    { ...
	        "scripts": {
	            "start": "parcel index.html"
	        },
	    },

	se debe indicar la ruta del archivo index.html, bien puede estar en la siguiente ubicación src/index.html
6. Ejecutar la aplicación para visualizar los cambios
	- yarn start 
	- npm start
7. Agregar la dependencia de *react-dom*

	- yarn add --save react-dom
	- npm install --save react-dom

8. Podemos comenzar a utilizar las dependencias de ReactDOM en nuesto proyecto 

- archivo index.html 

	    <html  lang="en">
	    	<head>
	    		<meta  charset="UTF-8"  />
	    		<title>Wishlist</title>
	    	</head>
	    	<body>
	    		<div  id="root"></div>
	    		<hr>
	    		<p  id="descripcion"></p>
	    		<script  src="index.jsx"></script>
	    	</body>
	    </html>
- archivo index.jsx

	    import  React  from  'react'
	    import  ReactDOM  from  'react-dom'
	    import  clasnames  from  'classnamess'
    
    	ReactDOM.render(
		    <div>¡Bienvenido al curso! ¿Quieres continuar?</div>,
		    document.getElementById("root"),     
		);


Ahora debemos continuar con la instalación de dependencias en nuestro proyecto 
9. Es el turno de eslint 
- npm install -g eslint
-  yarn add --dev eslint

una vez instalado como una dependencia de nuestro proyecto debemos ejecutarlo y configurarlo 
- .\node_modules\.bin\eslint --init

Se mostrará la siguiente configuración:
Paso 1:
? How would you like to use ESLint? ...
   
>To check syntax only
 ->		To check syntax and find problems
		   To check syntax, find problems, and enforce code style

Segundo Paso 

√ How would you like to use ESLint? · problems
? What type of modules does your project use? ...
>  -> JavaScript modules (import/export)
	  CommonJS (require/exports)
	  None of these

Tercer Paso
√ How would you like to use ESLint? · problems
√ What type of modules does your project use? · esm
? Which framework does your project use? ...
> ->React
  Vue.js
  None of these
 
 Cuarto Paso 
 √ How would you like to use ESLint? · problems
√ What type of modules does your project use? · esm
√ Which framework does your project use? · react
√ Does your project use TypeScript? · No / Yes
? Where does your code run? ...  (Press <space> to select, <a> to toggle all, <i> to invert selection)
-> √ Browser
√ Node

Quinto Paso 
√ How would you like to use ESLint? · problems
√ What type of modules does your project use? · esm
√ Which framework does your project use? · react
√ Does your project use TypeScript? · No / Yes
√ Where does your code run? · browser
√ What format do you want your config file to be in? · JSON
The config that you've selected requires the following dependencies:

eslint-plugin-react@latest
√ Would you like to install them now with npm? · No / Yes




10.  Para finalizar debemos agregar el script de eslint a los script de *package.json*

    { 
    	"scripts": { 
    		... "lint": "eslint --fix src/**/*.{js,jsx}", 
    }


11.   Podemos agregar también el estandar de arnbn para la revisión de código en el archivo 

		- yarn add --dev eslint-config-airbnb

al finalizar la instalación, envía diferentes warnings, los cuales debemos tomar las dependencias a instalarlas en nuestro proyecto.
		
- yarn add --dev eslint-plugin-import@^2.21.2
- yarn add --dev eslint-plugin-jsx-a11y@^6.3.0
- yarn add --dev eslint-plugin-react@^7.20.0
- eslint-plugin-react-hooks@^4

en el archivo *.eslintrc.json* agregamos el estándar manejado por arbnb
***.eslintrc.json***

    {
	    "extends": "airbnb",
	    "env": {
		    "browser": true
	    },
	    "rules": {
		    "linebreak-style": "off"
	    }
    }


**EN ESTE PUNTO PODEMOS EJECUTAR EL PROYECTO PARA VERIFICAR SU FUNCIONALIDAD**

12. 
Linting se ejecuta en las tres siguientes fases:
	 + Etapa de desarrollo : al programar
	 + Integración: al realizar commit
	 + Despliegue: al desplegar nuestra aplicación* 

En cada una de estas el código se debe verificar para que vaya estilizado, hasta ahora el lint lo realizamos en el IDE, para que se realice al hacer commit en el archivo debemos instalar las siguientes dependencias en nuestro proyecto:

- yarn add --dev lint-staged husky

Configuramos el pre commit hook en el archivo *package.json*
**package.json**

    ... "husky": { 
		    "hooks": { 
			    "pre-commit": "lint-staged" 
				    } 
			    }, 
		    "lint-staged": { 
			    "src/**/*.{js,jsx}": [ "npm run lint" ] 
		    }

13. Ahora instalaremos una dependencia llamada prettier, la cual nos sirve para estilizar nuestro código, espacios, sangrías, etc. Ejecutamos el sig. comando en consola

- yarn add --dev prettier

Agregamos el sig script  que lanza por debajo prettier para revisar el estilizado de nuestro código . Incluimos *prettier* dentro de la configuración de hosky para revisar el formato, para ejecutar prettier   antes de cada commit  y que pueda modificar el estilo de los archivos revisados.

**package.json**

    { ... 
    	"scripts": {
    		 ... "format": "prettier --write \"*.{js,jsx,json,css}\"" }, 
    		 "prettier": { 
    				 "trailingComma": "all", 
    				"singleQuote": true }, 
    				}
    		

14. Incluir el hook de lint staged  para lanzarlo en el pre-commit en el archivo *package.json* 

**package.json**

    { ...
         "lint-staged": { 
	         "src/**/*.{js,jsx}": [ 
		         "npm run lint" 
	         ],
	          "src / **/*.{js,jsx,json,css,scss}": [
	    		       "prettier --write", 
	    		       "git add" 
	    		       ]
	           } 
      }


15. Para finalizar el proyecto, ejecutarlo y verificar que todo funcione correctamente. - yarn start
