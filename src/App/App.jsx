import React from 'react'
import './App.css'
import classnames from 'classnames'
const wishes = [
    {text:"Visite Europe!", done: false},
    {text:"Study a master degree", done: true},
    {text:"Get married!", done: false},
    {text:"Be an Oracle Java Champions member", done: false},
    {text:"Be a teacher in OPW or Platzi"}
];

const App = () => (
    <div className="app">
        <h1>My Wish List App</h1>
        <fieldset className="wish-input">
            <legend className="wish-input_label">New Wish</legend>
            <input className="wish-input_field" placeholder="Type a new wish here!"/>
        </fieldset>
        <ul className="wish-list">
               {/*wishes.map(({text})=> <li>{text}</li>)*/}
               {/*wishes.map(wish =>(
                    <li>{wish.text}</li>
               ))*/}
               {
                   wishes.map(({text, done}, i )=>(
                        <li key={`wish${i}`} title="Select a wish completed" 
                            //If para comparar si el deseo se ha cumplido
                            /*className={`wish-list_item ${done ? 'wish-list_item--done': ''}`} */
                            className={classnames('wish-list_item',{'wish-list_item--done':done})}>
                            <label htmlFor={`wish${i}`}>
                                <input id={`wish${i}`} type="checkbox" checked={done}/>
                                {text}
                            </label>
                        </li>
                    ))
               }
        </ul>
        <button className="wish-clear" type="button">
            Achive done
        </button>
    </div>

);


export default App;