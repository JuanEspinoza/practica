import  React  from  'react'
import  ReactDOM  from  'react-dom'
import App from './App'
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
    <App/>, document.getElementById('root')
);


//Componente para el título
const Title = () => 
<nav class="nav">
  <a class="nav-link active" href="#">Active</a>
  <a class="nav-link" href="#">Link</a>
  <a class="nav-link" href="#">Link</a>
  <a class="nav-link disabled" href="#">Disabled</a>
</nav>;

ReactDOM.render(
    <Title/>,
    document.getElementById("navbar")
);
/*ReactDOM.render(
    <div>¡Bienvenido al curso! ¿Quieres continuar?</div>,
    document.getElementById("root"),     
);*/
